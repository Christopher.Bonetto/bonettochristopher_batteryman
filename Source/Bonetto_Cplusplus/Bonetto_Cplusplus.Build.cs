// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Bonetto_Cplusplus : ModuleRules
{
	public Bonetto_Cplusplus(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{ "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG" });
	}
}
