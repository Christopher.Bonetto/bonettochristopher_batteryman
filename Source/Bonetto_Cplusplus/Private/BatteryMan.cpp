// Fill out your copyright notice in the Description page of Project Settings.


#include "BatteryMan.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABatteryMan::ABatteryMan()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Get capsule component and give to it an initial size.
	GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);

	//Initialize starting movement variables.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.0f;
	GetCharacterMovement()->AirControl = 0.2f;

	//Create a new spring arm component and attach it to the RootComponent.
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);

	//Give to the spring arm component (CameraBoom) an arm length and tell him to follow the pawn rotation (i think).
	CameraBoom->TargetArmLength = 300.0f;
	CameraBoom->bUsePawnControlRotation = true;
	
	//Create a new camera component (named = FollowCamera) and attach it to the spring arm component.
	//Tell to him to don't follow the pawn rotation.
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	bDead = false;
	Power = 100.0f;
}



// Called when the game starts or when spawned
void ABatteryMan::BeginPlay()
{
	Super::BeginPlay();
	
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ABatteryMan::OnBeginOverlap);

	if(Player_Power_Widget_Class != nullptr)
	{
		Player_Power_Widget = CreateWidget(GetWorld(), Player_Power_Widget_Class);
		Player_Power_Widget->AddToViewport();
	}


	Player_AnimInstance_Ref = (GetMesh()) ? GetMesh()->GetAnimInstance() : nullptr;
	if(Player_AnimInstance_Ref != nullptr)
	{
		Player_ReplicationAnimInstance_Ref = Cast<UReplicationConceptsAnimInstance>(Player_AnimInstance_Ref);
	}
}

// Called every frame
void ABatteryMan::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckPower(DeltaTime);

	UpdateAnimation();

	
}

// Called to bind functionality to input
void ABatteryMan::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABatteryMan::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABatteryMan::MoveRight);
}

//Movements
void ABatteryMan::MoveForward(float Axis)
{
	if(!bDead)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Axis);

		this->MoveDirection.X = Axis;

		//Debug
		FString DebugMsg = FString::Printf(TEXT("Move forward value : %s"), *FString::SanitizeFloat(Axis));
		GEngine->AddOnScreenDebugMessage(1, 0.0f, FColor::Green, DebugMsg);
	}
}
void ABatteryMan::MoveRight(float Axis)
{
	if (!bDead)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Axis);

		this->MoveDirection.Y = Axis;

		//Debug
		FString DebugMsg = FString::Printf(TEXT("Move right value : %s"), *FString::SanitizeFloat(Axis));
		GEngine->AddOnScreenDebugMessage(2, 0.0f, FColor::Blue, DebugMsg);
	}
}

//Power
void ABatteryMan::CheckPower(float inTimeValue)
{
	Power -= inTimeValue * Power_Treshold;

	if (Power <= 0)
	{
		if (!bDead)
		{
			bDead = true;

			GetMesh()->SetSimulatePhysics(true);

			FTimerHandle UnusedHandle;
			GetWorldTimerManager().SetTimer(UnusedHandle, this, &ABatteryMan::RestartGame, 3.0f, false);

		}
	}
}

//Animation
void ABatteryMan::UpdateAnimation()
{
	if (Player_ReplicationAnimInstance_Ref != nullptr)
	{
		MoveDirection.Normalize();
		Player_ReplicationAnimInstance_Ref->TakeMovementSpeed(MoveDirection.Size());
	}
}

//Restart
void ABatteryMan::RestartGame()
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

//Collision
void ABatteryMan::OnBeginOverlap(UPrimitiveComponent* HitComp,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor->ActorHasTag("Recharge"))
	{
		UE_LOG(LogTemp, Warning, TEXT("Collided with"));

		Power += 10.0f;

		if(Power > 100.0f)
		{
			Power = 100.0f;
		}

		OtherActor->Destroy();
	}

}

