// Copyright Epic Games, Inc. All Rights Reserved.

#include "Bonetto_CplusplusGameMode.h"
#include "Bonetto_CplusplusCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABonetto_CplusplusGameMode::ABonetto_CplusplusGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
