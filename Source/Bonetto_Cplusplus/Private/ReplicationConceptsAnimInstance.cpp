// Fill out your copyright notice in the Description page of Project Settings.


#include "ReplicationConceptsAnimInstance.h"
#include "BatteryMan.h"
#include "GameFramework/CharacterMovementComponent.h"

UReplicationConceptsAnimInstance::UReplicationConceptsAnimInstance()
{
	Speed = 0.0f;
	isInAir = false;
}


void UReplicationConceptsAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	AActor* OwingActor = GetOwningActor();

	if(OwingActor != nullptr)
	{
		//Speed = OwingActor->GetVelocity().Size();

		ABatteryMan* OwingCharacter = Cast<ABatteryMan>(OwingActor);
		if(OwingCharacter != nullptr)
		{
			isInAir = OwingCharacter->GetCharacterMovement()->IsFalling();
		}
	}
}

void UReplicationConceptsAnimInstance::TakeMovementSpeed(float inValue)
{
	Speed = inValue * 600;

	FString DebugMsg = FString::Printf(TEXT("Move forward value : %s"), *FString::SanitizeFloat(Speed));
	GEngine->AddOnScreenDebugMessage(3, 0.0f, FColor::Red, DebugMsg);
}

