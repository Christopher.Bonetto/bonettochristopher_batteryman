// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Bonetto_CplusplusGameMode.generated.h"

UCLASS(minimalapi)
class ABonetto_CplusplusGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABonetto_CplusplusGameMode();
};



